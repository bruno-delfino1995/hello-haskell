-- ## Types

-- Despite of have explained the basic types on `basic-data-types.hs`, we should
-- go deeper on the understanding of types. Something that might still be blurry
-- is that datatype declarations define type constructors and data constructors,
-- the latter being responsible for constructing values of the type and the former
-- for specifying the type itself.
-- However we can think of those data constructors as functions, some might have
-- parameters and some not, that create data/values. Sometimes you might think that
-- we don't have to explict construct values, we can just put a number and there's
-- the value, but if you think as the number being a data constructor for numerical
-- types, you'll realize that in fact we are constructing values all the time and
-- that not a single value on Haskell is untyped. But what really seems to not have
-- a type are functions, however they do have a specific type which is ommited due
-- to syntax sugar.

-- For people coming from dynamic typed languages, or even worse dynamic and weaked
-- typed languages, types might seem unecessary, just bloathing our code. Although
-- types are really handy when it comes to imposing constraints and enforcing correctness
-- of the code, or even better, getting code errors before runtime through static
-- analysis. Haskell's type system has derived from *System F* from 1970s with additions
-- for general recursion and Hindley-Milner system for type inferences. With a well
-- designed type system, such as the one Haskell uses, we can eliminate a whole
-- class of errors which would happen during runtime, e.g., providing a String where
-- a Bool is expected.
-- However, no type system can eliminate errors that can't be statically checked,
-- such as data coming from a database, from user input, from networks, from file
-- system, .... Those kind of errors should be eliminated by testing your code and
-- ensuring that it handles them properly and any other exception that may arise.
-- Therefore, by having a type system we can reduce the amount of tests we need to
-- write.

-- ## How to read type signatures

-- On the REPL (GHCi) we can query for type signatures using `:t <type_to_inspect>`,
-- which will then output the value being inspect followed by it's type signature,
-- e.g., `:t 't' -- > 't' :: Char`. For those simple signature we can read them as
-- 't' has the type Char, we just need to substitute `::` by 'has the type'. However
-- when querying less concrete types, such as numeric values, we see the typeclass
-- instead of the type because GHC still has no idea of what type it needs to apply
-- on that polymorphic type so that it. The compiler uses the polymorphic first so
-- that we can use an Int on computations that require other type that is also a
-- Number, that's why GHC gives the most polymorphic type in a constrained way such
-- as `Num a => a` where `a` is polymorphic but constrained to a instance of `Num`.
-- That polymorphic behaviour is why we get `:t 13 -- > Num a => a` and
-- `:t (13 :: Integer) -- > Integer` when we force it to an Integer.
-- Besides normal variables, literals, full applied data constructors, ... we can
-- also query for function types, such as `not` which inverts a boolean, when
-- evaluating the type of `not` we'd get `Bool -> Bool`

-- ### Understanding the function type

-- When evaluating a type of a function we get an arror `->`, it's the type constructor
-- for functions which is baked into the Haskell language. The function type constructor
-- ressembles the tuple type constructor, however, unlike tuples, functions don't have
-- a data constructor for the term value, what is shown at the term level is already
-- the functions, because functions are values. The function constructor, as follows
-- `data (->) a b` receives two parameters and associates them to the right (but
-- application does the inverse), through this parametrization and association with
-- the notion of a function we see that the first parameter `a` is something that is
-- bound to the second `b` which taking for example the `not` function is the return
-- or result of that function.
-- When reading the type signature of a function such as `fst` we can get some hints
-- `(a, b) -> a`, by that signature we see that it gets a tuple of `a` and `b` and
-- returns the first, with that we can see that there's no transformation from the
-- first inhabitant of the tuple and the result, mainly because it's hard to imagine
-- a transformation that's 100% polymorphic without having a reference or a constraint,
-- such as `Num`.
-- Another example, for a type signature which somewhat manipulates its input, is
-- the `length` function with the signature as `[a] -> Int`, by that we can see that
-- there's an obvious manipulation from a list to an Int, however, we can see that
-- this function doesn't care about the inhabitants of the given list, so we can assume
-- that it probably doesn't use them at all.
-- However, be very carefull when doing assumptions on functions' type signatures,
-- some may seem very clear and obvious but it's always best to trust in the documentation,
-- e.g., a function that goes from `a` to `a` (`a -> a`) gives us no clue of what
-- it does, so on any doubt please refer to the documentation.

-- ### Typeclass-constrained type variables

-- When inspecting the signature for some numerical functions we usually get a type
-- constraint with a typeclass on it, e.g., `:t (+) -- > (+) :: Num a => a -> a -> a` says
-- that given two Num we can add them, the same applies to `:t (/) -- > (/) ::
-- Fractional a -> a -> a` which basically is the same but with Fractional types.
-- On this, we are basically constraining the available types that can be applied to
-- that variable to the types that have instances of such typeclasses. We get a polymorphic
-- (once it's a variable) and constrained type, that leads the compiler to assign
-- the most suitable type that can match the constraint (that has an instance for
-- that typeclass), often leading to the least specific and most generic type. It's
-- said that the type is *constrained* because we still don't know the concrete type
-- for that variable, but it can only be one that has an instance for the typeclass.

-- This instantiation of a typeclass doesn't only let us use the set of functions defined
-- on the typeclass. It also enables us to do some type conversions from the typeclass
-- constrained value to some of its instances as seen below.
fifteen = 15
fifteenInt = fifteen :: Int
fifteenDouble = fifteen :: Double
-- As `:t fifteen -- > Num a => a` we can convert fifteen to any of Num's instances,
-- however, once converted, thus specifing a concrete type, we can't do operations
-- for the Num typeclass based on two different instances. This is inherently impossible
-- because we would be breaking the type signature for Num's operations, as with `+`,
-- we'd be providing two different types where it expects both to be the same. By
-- the signature from `(+) :: Num a => a -> a -> a` Haskell would try to give *a* a concrete
-- type based on the given arguments, however, as you provide `Int` and `Double`
-- (two different concrete types) Haskell won't be able to know which one to use
-- once there is no common factor between them, it'd apply Int to the first *a*, and
-- thus expecting Int for the second as weel once both are based on the same type variable.
-- fifteenInt + fifteenDouble -- fails (+) even without the constraint doesn't
-- consider two different variables, it's `a -> a -> a` and not `a -> b -> a`
-- Although, if we try a computation with one of the concrete types and the generic one
-- Haskell'd convert it automatically to the concrete type specified before once it's
-- an instance of the generic one, thus `fifteenInt + fifteen` is valid because *fifteen*
-- can be converted to Int.
-- Besides this simple constraint with only one type, we can have a type variable
-- constrained by more than one typeclass, or two variables constrained by the same
-- typeclass. It'll depend on how you declare your type constraint, if you specify
-- the same variable on more than one typeclass, as in `(Ord a, Num a) => a -> a -> Ordering`,
-- then the variable *a* needs to be populated by a concrete type that is both an instance
-- of Ord and an instance of Num, therefore, if you specify `(Num a, Num b) => a -> b -> b`,
-- then we have two variables, *a* and *b*, that need to have instances of the same
-- typeclass Num.

-- ### Exercises: Types Matching

-- 1a matches 2c -> not :: Bool -> Bool -- from Bool to Bool
-- 1b matches 2d -> length :: [a] -> Int -- from List of a to Int
-- 1c matches 2b -> concat :: [[a]] -> [a] -- from List of List of a to List of a
-- 1d matches 2a -> head :: [a] -> a -- from List of a to a
-- 1e matches 2e -> (<) :: Ord a => a -> a -> Bool -- compares ordered items

-- ## Currying

-- As in lambda calculus, Haskell takes only one argument and return a result, when
-- we have many arguments in a function definition what happens is that for each
-- applied argument we return a new function with the parameter bound to argument's value.
-- This behavior of having nested functions each accepting one argument and returning
-- another function until we apply all the arguments and get a value is what we call
-- *currying*, thus allowing the illusion of having multiple parameters functions.
-- The arrow which is used for function definition has the following signature `data (->) a b`
-- which represents a single input and some result, no matter what that result or input
-- can be. Each arrow when declaring a function denotes an input and a result, so
-- `(<) :: Ord a => a -> a -> Bool` denotes two functions that when grouped would
-- result into `(<) :: Ord a => a -> (a -> Bool)`, this grouping being defined by
-- the right associativity of the arrow `->`. Because of this currying we can create
-- partially applied functions and pass them to other functions, such as adding 2 to
-- any number in a map.
add2ToArr = map (+ 2)
-- Above we have two curried functions, the first being the (+) operator applied
-- to 2 which remove the necessity of declaring a lambda for the second parameter,
-- such as `\x -> x + 2`. The second curried function is `map` because the array/list
-- parameter is pending, this way we're able to declare what's called a point free
-- declaration once we haven't declared its parameters and have declared it in means
-- of other functions composition.

-- ## Partial Application