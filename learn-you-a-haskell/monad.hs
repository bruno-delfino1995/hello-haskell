import Control.Monad.Writer
-- Until now we have been dealing with a bunch of type classes. Things that are
-- within a context and can be mapped over with normal functions (Functor), things
-- that have both the value and the function within a context and we map one over
-- the other (Applicatives), things that can be combined and have an identity value
-- (Monoid) and finally things that can be folded to one single value (Foldable).
-- Now we are going to work with values, within contexts, that need to feed functions
-- which receive only the value, without the context, and return a new value within
-- the same context. For dealing with that we have Monads with the bind operation `>>=`,
-- monads are mainly concerned with the previous case, and have all the abilities of
-- Applicative Functors.

-- The main logic is pretty simple, lets imagine it for `Maybe`, if you think about
-- the context of a Maybe, Just x or Nothing, you quickly realize that if the value
-- to be feed into the function is Nothing we just return it, otherwise we unwrap
-- and apply.
applyMaybe :: Maybe a -> (a -> Maybe b) -> Maybe b
applyMaybe Nothing f = Nothing
applyMaybe (Just x) f = f x

monadSum = Just 5 `applyMaybe` (Just . (+ 1))
nothingComesInNothingGoesOut = Nothing `applyMaybe` (Just . (+ 1))
don'tCareAboutResult = Just 5 `applyMaybe` (\_ -> Nothing)
cascade y = Just y `applyMaybe`
            (\x -> if x > 2 then Just x else Nothing) `applyMaybe`
            (Just . (+ 1)) `applyMaybe`
            (\x -> if x < 5 then Nothing else Just x) `applyMaybe`
            (Just . (subtract 1))

-- Let's think about computations that return some value and then another computation
-- which depends on that value just needs to be run next to it. Think about a tightrope
-- walker which is holding a balancing pole that receives birds in an aleatory manner
-- over time, and depending on the difference of birds between the side he may fall.
-- This represents a computation where we have one previous event - a bird landing in
-- one side of the pole -- that influence the walker's balance, which will influentiate
-- the next bird's landing as well
type Birds = Int
type Pole = (Birds, Birds)

-- First we begin with the landing functions
landLeft :: Birds -> Pole -> Pole
landLeft n (left, right) = (left + n, right)

landRight :: Birds -> Pole -> Pole
landRight n (left, right) = (left, right + n)

firstWalk :: Pole -> Pole
firstWalk = landLeft 2 . landRight 1 . landLeft 1

-- Of course we could make that be read as a landing sequence from left to right
-- by creating a new operator
(-:) :: a -> (a -> b) -> b
x -: f = f x
firstWalk' pole = pole -: landLeft 1 -: landRight 1 -: landLeft 2

-- Now think about the case where the walker loses its balance, lets think that he
-- would lose the balance when the difference of birds is more than 4. And if the
-- walker have already lose its balance, then the landing of birds becomes impossible.
-- In this case we are introducing a failure scenario, a missing value, a context where
-- may be none inside of it, a.k.a, a Maybe. Now we can create our balance functions
-- using this context of failure. If we tried that in the older way it would be with a lot
-- of if checks and the code would look a mess, because we would need to check on any
-- landing if the walker has lose its balance.
landRight' :: Birds -> Pole -> Maybe Pole
landRight' n pole@(left, right)
    | abs (left - (right + n)) < 4 = Just (landRight n pole)
    | otherwise = Nothing

landLeft' :: Birds -> Pole -> Maybe Pole
landLeft' n pole@(left, right)
    | abs ((left + n) - right) < 4 = Just (landLeft n pole)
    | otherwise = Nothing

-- Here we created our chain of functions where one depends on the output of the before.
-- Notice that we have used `return` to put the `Pole` in a the context used aftwerwards,
-- so that it can be handled by the `>>= - 'bind'` operator
secondWalk :: Pole -> Maybe Pole
secondWalk pole = return pole
    >>= landLeft' 1
    >>= landRight' 4
    >>= landLeft' (-1)
    >>= landRight' (-2)

-- One thing that we've used before and that is closely related to Monads is the
-- `do` notation. It's just a helper for extracting our monadic values into a name
-- and applying that to a function, that is basically what >>= does.
-- The following code is a common example of cases in which we would like to use
-- the intermediate value to create a new one, or bind it before returning. Here
-- we can see that *"first"* we turn our 3 into a "!" and then use that "!" to
-- concat it with the latter 3. But this using parentheses is pretty ugly if we choose
-- to take advantage of that Haskell doesn't know where a lambda finishes can get a
-- little bit confusing
foo :: Maybe String
foo = Just 3   >>= (\x ->
      Just "!" >>= (\y ->
      Just (show x ++ y)))

foo' :: Maybe String
foo' = Just 3 >>= \x -> Just "!" >>= \y -> Just (show x ++ y)

-- I'm still not sure if we can use different monads inside of a do notation, but
-- I'm pretty sure that no because we can't change the shape of a monad while binding it.
-- The signature of `>>=` is `Monad m -> m a -> (a -> m b) -> m b` instead of
-- `Monad m, Monad n -> m a -> (a -> n b) -> n b`
doFoo :: Maybe String
doFoo = do
    x <- Just 3
    y <- Just "!"
    return (show x ++ y)

doIsSameAsBind = doFoo == foo'

-- One useful feature that the do notation have is the ability to automatically
-- call `fail :: Monad m => String -> m a` if the internal code has any runtime error,
-- such as unexaustive pattern matching. In the following example we used the Maybe
-- Monad which the implementation for `fail` is just `Nothing`'
doWithRuntimeErr = do
    (x:xs) <- Just ""
    return x

-- Monads, as Functors and Applicatives, have its own laws as well, which every instance
-- must hold
f = Just
g = Just . (+ 1)
leftIdentity = (return 1 >>= f) == f 1
rightIdentity = (Just 1 >>= return) == Just 1
associativity = (Just 1 >>= f >>= g) == (Just 1 >>= (\x -> f x >>= g))
isMaybeAValidMonad = leftIdentity && rightIdentity && associativity

-- One useful monad that adds the logging context is the Writer monad, basically
-- it's composed by a value and a monoid describing something that can be combined.
-- That monoid can be useful for one of the most uses of the Writer monad which is
-- logging, but also can be used to compute a cart where items get added along the
-- flow. For the logging case, we normally use the [] Monoid and for the cart we would
-- end up using the Sum Monoid because the end result needs to be a sum of everything.
-- Below we are using that monad to calculate the greatest common divisor along with
-- a logging of the operations. We used the [] Monoid because if we had used the String
-- Monoid we would end with a long string without any line break, behind the scenes
-- we would be using the [] Monoid also, but it'd be an list of chars, ending in a String
gcd' :: Int -> Int -> Writer [String] Int
gcd' a b
    | b == 0 =
        tell ["Finished with " ++ show a]
        >> return a
    | otherwise =
        tell [show a ++ " mod " ++ show b ++ " = " ++ show (a `mod` b)]
        >> gcd' b (a `mod` b)
-- Above we have used the >> instead of >>= which basically ignores the context value
-- but keeps the context. Which is exactly what we wanted, because tell returns a writer
-- with an unit (`()`), and we don't want to feed that to the next computation, although
-- we want to keep the context because `tell` is describing what is going to happen.