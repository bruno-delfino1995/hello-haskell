-- Lists like other languages are defined using square brackets and elements separated
-- by comma, unlike some languages Haskell's lists are homogenous - they can have only a
-- single type.
lostNumbers = [4, 8, 15, 16, 23, 42]

-- Like Elixir, when we append to a list using the `++` operator we have to walk
-- through the entire list, that can be painfull if we work with million elements lists
-- but for short lists we can use that
newLostNumbers = lostNumbers ++ [56, 45]

-- On the other side, when we prepend to a list using the `:` (cons operator) we just
-- have to create a new list element and point its tail to the specified list
oldLostNumbers = 5 : lostNumbers

-- And like Elixir, lists are recursive strutures, making `[1, 2, 3]` be just a syntactic
-- sugar for `1:2:3:[]`
syntaticSugar = [1, 2, 3] == 1:2:3:[]

-- Haskell has a handy operator for getting an element at a certain index, if we want
-- to get the sixth element from `lostNumbers` we just have to use the `!!` infix operator
-- with the list and the number
sixth = lostNumbers !! 6

-- Also we can specify ranges of number, let's say we want a list from 1 to 20, instead
-- of typing every number we could specify a range
oneToTwenty = [1..20]

-- Beyond of that we can also specify a single step for the range, like every multiple
-- of three from 1 to 20. Although we can't specify more than one step, we can't do
-- things like specifying a list with all powers of 2 by making [1,2,3,4..20].
multiplesOfThree = [3,6..20]

-- Another thing to notice is that by default ranges are ascending, so if you want
-- a list from 20 to 1 you have to specify a decrementing step
fromTwentyToOne = [20,19..1]

-- If we can build a list from another list we can use comprehensions for that,
-- comprehensions are composed by an output function and a list generator
firstTenDoubled = [x * 2 | x <- [1..10]]

-- Also we can filter our list generator elements by using a predicate after the
-- generator
firstTenOdd = [x | x <- [1..10], odd x] -- here we have intentionally put a bug in the code, we have only the odd numbers to 10
oddNumbers = [x | x <- [1..], odd x] -- here we will filter all the odd numbers starting from 1

-- One thing that should bare at every functional programmer's mind is to use small
-- pieces together to build greater parts, so by spliting the first ten odd number
-- into an infinite list and a call to `take` we can reason about our solution much easier
firstTenOdd' = take 10 oddNumbers

-- In list comprehensions we can use multiple predicates and multiple generators as well
-- You just need to separate them by a comma, but remember to keep the generators grouped
-- and before the predicates, because we have to bound the generators to variables first.
-- We can put them in any order if the variables are already bound to be used in the
-- predicate, but for better readability is better to group them. Another thing to
-- notice is that only the elements that satisfy all the predicates will be part of
-- the comprehension result. And last but not least, all the generator will be combined
-- like in a cartesian product
-- [x * y | x <- [2,5,10], x > 2, y <- [8,10,11], x * y > 50] -- bad
-- [x * y | x <- [2,5,10], y <- [8,10,11], x > 2, x * y > 50] -- good

-- Remember that string are lists of characters, so we can use comprehensions for them too
removeNonUppercase :: String -> String -- Here we have defined that `removeNonUppercase` is a
-- function that takes a String and returns a String
removeNonUppercase st = [x | x <- st, x `elem` ['A'..'Z']]

-- Also we can use comprehensions to build list of lists
evenNumbersWithinAList = let xxs = [[1,3,5,2,3,1,2,4,5],[1,2,3,4,5,6,7,8,9],[1,2,4,2,1,6,3,1,3,2,3,6]] in
    [ [ x | x <- xs, even x ] | xs <- xxs]

-- Define `length` using list comprehensions
length' xs = sum [1 | x <- xs]