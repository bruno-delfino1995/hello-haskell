import qualified Data.Char as C
import Control.Monad -- for mapM_ and forM
import System.IO

main = nameAsker
-- Haskell is a compiled language, until now we have used only the GHCi interpreter.
-- Now we need to compile our program and execute it like a normal executable. For
-- that we use just the compiler by typing `ghc --make file`, this will create
-- an executable binary that runs our main funcion by default
-- main = putStrLn "hello, world!"

-- By looking at the type signature of `putStrLn` we see that it gets a String and
-- returns `IO ()` also know as an IO action with the result type of an empty tuple
-- `()` - know as unit. IO acions are used to represent things that have side-effects
-- when performed, in this case the result value is a dummy unit because printing
-- to the screen doesn't have a meaningful return value.
-- You may ask how does an IO action gets performed, that happens when we give it
-- a name of `main` and then run the program. Although our previous main definition
-- has just one IO action we can use the `do` syntax (like a new language inside haskell),
-- to glue a bunch of IO actions together. `do` syntaxes are made for side-effects,
-- one of the features of it is that the code will be tainted, and pure functions
-- will not be able to call it, only the inverse.
nameAsker = do
    putStrLn "Hello, what's your name?"
    name <- getLine
    putStrLn ("Hey " ++ toUpper name ++ ", you rock!")

toUpper :: [Char] -> [Char]
toUpper = map C.toUpper

-- When we use the `do` syntax we just put steps each one returning an IO action,
-- and the type of main will be the type of the last IO action.
-- In the last main definition we see a new IO action, the one returned by `getLine :: IO String`,
-- and along with it we see the `<-` construct, which simply unwraps the value gathered
-- by the IO action and bind it to the label you gave to it. In that case the `<-`
-- will perform the IO action and bind the result to name, once `getLine` has the type
-- of `IO String`, so `name` will be a `String`.
-- Another point of the `<-` construct is that it can only be used to unwrap an IO action
-- when inside another IO action, we can use it only within impure code - by impure
-- here you can think of something that isn't predictable, if performed twice maybe
-- will bring different data
-- However you can use the name bound to the result of the IO action as an argument
-- to any other function, because we temporarily un-taint the data to further usage. If
-- you want to deal with the tainted data you have to unwrap it, to do so you
-- have to do it inside an impure environment - another IO action.

-- From learnyouahaskell.com
-- I/O actions will only be performed when they are given a name of main or when
-- they're inside a bigger I/O action that we composed with a do block. We can also
-- use a do block to glue together a few I/O actions and then we can use that I/O action
-- in another do block and so on. Either way, they'll be performed only if they
-- eventually fall into main.

-- Remember that we can use let bindings to bind values to names and use them inside
-- the in block. Although for list comprehensions we don't need them because the
-- comprehension body is equivalent to the in block, for `do` blocks we can use
-- let bindings without the `in` as well. That way the names which were bound to
-- within the let bindings are available for usage in the remaining part
main' = do
    putStrLn "What's your first name?"
    firstName <- getLine
    putStrLn "What's your last name?"
    lastName <- getLine
    let bigFirstName = map C.toUpper firstName
        bigLastName = map C.toUpper lastName
    putStrLn $ "hey " ++ bigFirstName ++ " " ++ bigLastName ++ ", how are you?"

-- However let bindings are equal to `<-` contructs, remember that let bindings
-- are for variable definitions and <- constructs are for extracting the result out
-- of IO actions. One cool point to notice is that every IO action that you execute
-- in the GHCi it will automatically perform and unwrap its value

recursiveMain = do
    line <- getLine
    if null line
        then return ()
        else do
            putStrLn $ reverseWords line
            recursiveMain

reverseWords :: [Char] -> [Char]
reverseWords = unwords . map reverse . words

-- In this recursive main you can see two aspects. One is that, since every if is an
-- expression and should return a value, and also that `do` blocks are used to glue
-- IO actions together, the common pattern for `if` inside of `do` blocks is
-- `if x then IO y else IO z. Other is that we have glued two IO actions inside of
-- the else clause using a `do` block inside of another, and one of the actions
-- is a call to the function itself making it recursive and valid to the do block
-- because it at some time will return an IO action
-- You might be thinking that the return statement is ending our function by acting
-- like a return in other languages, however `return` in Haskell isn't like return in
-- other languages, it simply creates an IO action for the given value. This way we
-- are able to end the recursion with an IO action. The program ends because we
-- ended the recursion, not because we called return

returnIsNotReturn = do
    unit <- return ()
    name <- return "Return"
    explanation <- return "creates an IO action"
    putStrLn $ name ++ " " ++ explanation
    return "Can be used to make an IO action with a desired result"
    let useless = "If you are using <- with return"
        useful = "just use let bindings instead of y <- return x"
    return $ useless ++ " " ++ useful

associateColors = do
    colors <- forM [1, 2, 3] (\n -> do -- Maps values to IO actions and unwrap the array of values using <-
        putStrLn $ "Which color do you associate with " ++ show n ++ "?"
        getLine)
    putStrLn "The colors you have associated are:"
    mapM_ putStrLn colors -- Map values to IO actions just with a different order
    -- mapM and mapM_ are the same, the difference is that mapM_ returns IO () and mapM IO [()]

yellAtMe = do
    putStrLn "Tell anything that I'll answer you yelling it"
    forever $ do
        str <- getLine
        putStrLn . toUpper $ str

readItAll = do
    contents <- getContents -- getContents returns IO [Char] instead of IO [String] where each String is a line
    putStrLn . toUpper $ contents

interactWithUser = interact toUpper -- `interact` can be used to input/output programs, it reads and then prints

checkPalindromes = interact respondPalindromes

respondPalindromes :: String -> String
respondPalindromes = unlines . map (\line ->
    if isPalindrome line
        then "palindrome"
        else "not palindrome") . lines
    where isPalindrome str = str == reverse str

echoFile = do
    handle <- openFile "song.txt" ReadMode
    contents <- hGetContents handle
    putStrLn contents
    hClose handle