maximum' :: (Ord a) => [a] -> a
maximum' [] = error "Maximum of an empty list"
maximum' [x] = x
maximum' (x:xs)
    | x > maxTail = x
    | otherwise = maxTail
    where maxTail = maximum' xs

replicate' :: (Num a, Ord a) => a -> a -> [a]
replicate' 0 x = []
replicate' times x = (x:replicate' (times - 1) x)

take' :: (Num i, Ord i) => i -> [a] -> [a]
take' _ [] = []
take' n _ | n <= 0 = []
take' n (x:xs) = (x:take' (n - 1) xs)

reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

-- You can use recursion to create infinite lists, Haskell will chop it when asked for
-- due to its lazy evaluation behavior
repeat' :: a -> [a]
repeat' x = (x:repeat' x)

zip' :: [a] -> [b] -> [(a, b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = ((x,y):zip' xs ys)

elem' :: (Eq a) => a -> [a] -> Bool
elem' n [] = False
elem' n (x:xs)
    | n == x = True
    | otherwise = n `elem'` xs

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
    let smaller = quicksort [y | y <- xs, y <= x]
        greater = quicksort [y | y <- xs, y > x]
    in smaller ++ [x] ++ greater