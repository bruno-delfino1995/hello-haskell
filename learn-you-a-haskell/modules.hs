-- We can define our own modules by using this expression, the parentheses being
-- where we place the public functions to further on be exported
module Modules (numUniques) where
-- When you need the functions from a module in your local scope you just have to
-- import it and you get them available. Importing modules must be done before
-- defining any function, so usually it's done at the top of the file after module definition
import Data.List
-- When importing modules that you've defined, the file/folder has to be placed
-- in the same directory of the claimant
import Geometry.Sphere
-- We can:
-- - import just some of the functions `import Data.List (nub, sort)`
-- - don't import some to prevent name clashes `import Data.List hiding (nub)`
-- - import with full the same to prevent clashes `import qualified Data.Map`
--  - this prevents clashes like Map's filter with Prelude's filter, you gonna call
--    as `Data.Map.filter`
-- - give a alias for qualified imports `import qualified Data.Map as M`, shortening
--   the calls -> `M.filter`

numUniques :: (Eq a) => [a] -> Int
numUniques = length . nub -- Here nub is from Data.List, as we imported it becomes available

sphereVolume = volume
