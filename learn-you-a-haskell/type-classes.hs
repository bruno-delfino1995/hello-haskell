-- Type classes in Haskell are like interfaces in Java and protocols in Elixir/Clojure,
-- they specify a behavior to be implemented by specific types. This is the way
-- for us to be polimorphic.
-- Each typeclass is defined as following, in which they want a type variable that
-- will play the role of that 'class', e.g., in the declaration below the `a`
-- will behave as an instance of `Eq`
-- ```haskell
-- class Eq a where
--     (==) :: a -> a -> Bool
--     (/=) :: a -> a -> Bool
--     x == y = not (x /= y)
--     x /= y = not (x == y)
-- ```
-- One interisting thing to notice is the definition of `==` by means of `/=`, this
-- lets us define just one of them and the other will behave as expected, e.g., imagine
-- implementing all the differences in a semaphore, we would end with a lot of comparisions
-- for that we just define the equalside and the other would be 'automatically' implemented.
data Semaphore = Red | Yellow | Green

-- You implement the concrete side of the typeclass as follow, but for simple classes
-- as Eq it can be automatically implemented by deriving it, how Haskell does it I don't know
-- TODO: understand how to create a typeclass capable of being derived
instance Eq Semaphore where
    Red == Red = True
    Yellow == Yellow = True
    Green == Green = True
    _ == _ = False

-- Although we can auto-implement some classes they have a default behavior when
-- done so, to use a different behavior we must implement our own instance
instance Show Semaphore where
    show Red = "Stop"
    show Yellow = "Caution"
    show Green = "Go go go" -- When implementing the instance we can use each of those facilities
    -- of when defining functions

-- You can also put constraints when declaring your typeclasses by using constraints on
-- the declaration, lets say you want to implement a typeclass of only numbers that
-- has implemented a instance of Eq, it would be like this
-- class (Eq a) => Number a where

-- When declaring or using typeclasses we have to be aware of the type constructor
-- which the class is waiting, like in the Eq example, the Eq class expects a
-- concrete type - a type with all of its parameters applied, or without any - which
-- you can check on GHCi by using `:k Type`, whether that returns `*` it means that you
-- has gotten a concrete type, if not and you have gotten something like a function
-- then that means you still have some type parameters to be applied. By example,
-- if you try to get the kind of `Maybe` you would get `* -> *` as your result,
-- which means that it can't be used in classes that want a concrete type, but
-- can be used in `Functor` which waits for a type of that kind
class Functor' f where
    -- By checking this type declaration we can see that f has one field, meaning
    -- that f has the kind of `* -> *`, which represents that it wants at least one
    -- field
    fmap :: (a -> b) -> f a -> f b
    -- As types are just functions and can be partially applied you can pre apply
    -- some of the fields to get the type expected by your typeclass

data Nullable a = Null | Some a
instance Functor' Nullable where
    fmap _ Null = Null
    fmap f (Some x) = Some $ f x

-- `:k Barry` => (* -> *) -> * -> * -> *, Obs.: t is a type that will receive k as parameter
data Barry t k p = Barry { yabba :: p, dabba :: t k }
-- Here we partially applied the types a and b to Barry, which are flexible as they
-- are defined with type variables, we could lock them by passing a concrete type
-- like Int
instance Functor' (Barry a b) where
    fmap f (Barry {yabba = x, dabba = y}) = Barry { yabba = f x, dabba = y}